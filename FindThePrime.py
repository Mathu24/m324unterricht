# Eingabe der maximalen Zahl
max_num = int(input("Bitte geben Sie die maximale Zahl ein: "))

# Funktion zur Überprüfung, ob eine Zahl eine Primzahl ist
def is_prime(num):
    if num <= 1:
        return False
    if num <= 3:
        return True
    if num % 2 == 0 or num % 3 == 0:
        return False
    i = 5
    while i * i <= num:
        if num % i == 0 or num % (i + 2) == 0:
            return False
        i += 6
    return True

# Finden und Ausgeben der Primzahlen bis zur maximalen Zahl
print("Primzahlen bis zur maximalen Zahl:")
for num in range(2, max_num + 1):
    if is_prime(num):
        print(num)


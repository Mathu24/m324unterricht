# Eingabe der Temperatur in Grad Celsius
celsius = float(input("Bitte geben Sie die Temperatur in Grad Celsius ein: "))

# Umrechnung von Celsius in Fahrenheit
fahrenheit = (celsius * 9/5) + 32

# Ausgabe des Ergebnisses
print(f"Die Temperatur {celsius} Grad Celsius entspricht {fahrenheit} Grad Fahrenheit.")

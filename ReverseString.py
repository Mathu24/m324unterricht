# Eingabe des ursprünglichen Strings
original_string = input("Bitte geben Sie einen String ein: ")

# Umkehren des Strings
reversed_string = original_string[::-1]

# Ausgabe des umgekehrten Strings
print(f"Der umgekehrte String lautet: {reversed_string}")
